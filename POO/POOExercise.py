import math

class Point():
    x = int
    y = int

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "x: " + str(self.x) + ",y: " + str(self.y)

    def distanceTo(self, point):
        return math.ceil(math.sqrt(math.pow(point.x - self.x, 2) + math.pow(point.y - self.y, 2)))

class Triangle():
    point_A = Point
    point_B = Point
    point_C = Point

    def __init__(self, point_A, point_B, point_C):
        self.point_A = point_A
        self.point_B = point_B
        self.point_C = point_C

    def typeOf(self):
        distanceAB = self.point_A.distanceTo(self.point_B)
        distanceAC = self.point_A.distanceTo(self.point_C)
        distanceBC = self.point_B.distanceTo(self.point_C)

        if ((distanceAB == distanceAC) & (distanceAB == distanceBC) & (distanceAC == distanceBC)):
            return "Equilatero"
        elif ((distanceAB != distanceAC) & (distanceAB != distanceBC) & (distanceAC != distanceBC)):
            return "Escaleno"
        else:
            return "Isoceles"

class Circle():
    center = Point
    radious = int

    def __init__(self, point, radious):
        self.center.x = point.x
        self.center.y = point.y
        self.radious = radious

    def isInside(self, point):
        if (point.distanceTo(self.center) < self.radious):
            return True
        return False