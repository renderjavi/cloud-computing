import pytest
from POO.POOExercise import *

@pytest.mark.parametrize("p1 , p2, resp", [(Point(0, 1), Point(0, 0), 1)])
def test_distanceTo(p1, p2, resp):
    assert p1.distanceTo(p2) == resp

@pytest.mark.parametrize("p1 , p2, p3, tipo_triangulo", [(Point(-1, -1), Point(5, -1), Point(2, 5), "Isoceles")])
def test_typeOf(p1, p2, p3, tipo_triangulo):
    triangle = Triangle(p1, p2, p3)
    assert triangle.typeOf() == tipo_triangulo

@pytest.mark.parametrize("p1 , p2, p3, tipo_triangulo", [(Point(-1, 1), Point(5, -1), Point(2, 2), "Escaleno")])
def test_typeOf(p1, p2, p3, tipo_triangulo):
    triangle = Triangle(p1, p2, p3)
    assert triangle.typeOf() == tipo_triangulo

@pytest.mark.parametrize("center , p, radious, resp", [(Point(0, 0), Point(1, 1), 5, True)])
def test_isInside(center, p, radious, resp):
    circle = Circle(center, radious)
    assert circle.isInside(p) == resp
